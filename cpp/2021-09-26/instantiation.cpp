#include <iostream>
#include <vector>

struct MyClass {
    int x;
    MyClass(int y) { x = y; }
};

int main() {
    // since C++11, use brace-initialization

    /*
    MyClass var(); // function declaration
    ↓
    MyClass var{};

    MyClass var;  // might not be initialized
    ↓
    MyClass var{};

    MyClass var(12.5);  // implicit conversion to 12 loses precision
    ↓
    MyClass var{12.5};
    */

    // initialize v with four values 1, 2, 3, 4
    /*
    std::vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    */

    // vector constructor
    // initialize v with four values 5, 5, 5, 5
    //std::vector<int> v(4, 5);

    // initialize v with four values 1, 2, 3, 4
    std::vector<int> v = {1, 2, 3, 4};
    // same as
    //std::vector<int> v{1, 2, 3, 4};

    for (int x : v) {
        std::cout << x << "\n";
    }
}
