#include <iostream>

/*
C-style:
(T) V  // no check at all
// similar to reinterpret_cast

C++-style casts:

static_cast
dynamic_cast
const_cast
reinterpret_cast
bit_cast  // recent

// int to float: memcpy()
*/

template <typename T>
struct MyContainer {
  T* data[100];
  
  T* at(size_t index) /* non-const */ { // called with this of type MyContainer*
    const MyContainer* const_this = this;
    const T* const_ret = const_this->at(index);  // calls the other at
    T* non_const_ret = const_cast<T*>(const_ret);  // dangerous, but we know that the object is modifiable
    return non_const_ret;
  
    // return data[index];
  }

  // overload of at
  const T* // return type
  at // method name
  (size_t index) // arguments
  const  // const specifier for this
  {  // called with this of type const MyContainer*
  
    /*
    MyContainer* non_const_this = const_cast<MyContainer*>(this);  // might be undefined behavior!
    T* non_const_ret = non_const_this->at(index);  // calls the other at
    const T* const_ret = non_const_ret;
    return const_ret;
    */

    return data[index];
  }
  
  /*
                ↓ this one
  const T* at(const MyContainer* this, size_t index) {
    
  }
  */
};

template <typename T>
void f(const MyContainer<T>& v) {
  const T* t = v.at(0);
  std::cout << "The pointer of t is " << t << "\n";
}

int main(void) {
  MyContainer<int> v;
  int x;
  v.data[0] = &x;
  f(v);
  
  int* y = v.at(0);
  *y = 4;
}
