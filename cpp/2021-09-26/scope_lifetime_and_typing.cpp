int main() {
    /* will not compile because C++ is (mostly) strongly typed
    auto x = "bonjour";  // const char*
    x = 3;  // try to put int into const char*
    */

    /*
    Dynamic typing: type of a variable can change during execution (e.g. Python)
    Static typing: type of the variable is set at compile-time (e.g. C++)

    Weak typing: JavaScript
        'a' + 3 → 'a3'
        123 == '123' → true
        123 === '123' → false
    Mostly strong typing: Python, C++
        int x = std::string{"Hello World"};  // does not compile
        double x = 4;  // OK
    Strong typing: types are not converted implicitly (e.g. OCaml)
        1 → int
        1. → float
        1 + 2 → OK
        1 + 2. → does not compile because + takes ints and no implicit conversion
        1. +. 2. → OK
        1 +. 2. → does not compile because +. takes floats and no implicit conversion
        (float_of_int 1) +. 2. → OK because explicit conversion
    */

    /*
    C++: scoped lifetime
        destructors are always called when leaving scope
        new creates an object with longer lifetime (returns a pointer)
        delete finishes the lifetime of the object
        no garbage collection → beware memory leaks!
        manual memory management → beware delete the same object twice!
    Python: garbage collected
        objects might or might be freed when leaving scope
        garbage collection → beware stalls (the gc pauses the execution to find what can be cleaned up)
        contexts (with …:) ensure __exit__ is called when leaving the block
    */
}
