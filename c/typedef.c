#include <stdio.h>
#include <stdlib.h>

/*
typedef X Y;
using Y = X;
*/
struct Point {
    int x;
    int y;
};

typing 

void print(Point p) {
    printf("x=%d y=%d\n", p.x, p.y);
}

void f1(Point p) {
    p.x = 4;
    print(p);
}

void f2(Point* p) {
    p->x = 4;
    print(*p);
}

int main(void) {
    Point p = {
        .x = 1,
        .y = 2,
    };

    print(p);
    f1(p);
    print(p);
    f2(&p);
    print(p);

    return 0;
}
