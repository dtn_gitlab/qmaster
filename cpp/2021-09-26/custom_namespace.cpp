// custom namespace

namespace mon_namespace {
    struct MyClass {
        int x;
    };
}

int main() {
    mon_namespace::MyClass var{};
    (void) var;
}
