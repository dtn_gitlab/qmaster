#include <iostream>

using namespace std;

/*
struct Base {
  struct {
    void(*f)(void); // f is a pointer to a function void*(void)
    // std::function<void(void)> *f;
  } vtable;
};
struct Child1 {
  struct Base base;
};
struct Child2 {
  struct Base base;
};

void child1_f(void) {
  puts("virtual: Child1")  
}
Child1* child1_new(void) {
  Child1* ret = malloc(sizeof(Child1));
  if (ret == NULL) {
    // format hard drive and send nukes
  }
  ret->base.vtable.f = child1_f;
  return ret;
}
int main(void) {
  Child1* c1 = child1_new();
  Base* b = &c1.base;
  b->vtable.f(); // child1_f
}
*/

// pure virtual class
class Base {
  public:
    void base(void) { cout << "The base interface is Base\n"; }
    void f() { cout << "Base\n"; }
    virtual void vf() { cout << "virtual: Base\n"; } // virtual method
    virtual void pvf() = 0; // pure virtual method
};
// virtual : la méthode à appeler est déterminée à l'exécution
// virtual table ou vtable

/* Child1 is-a Base */
class Child1 : public Base {
  public:
    void f() { cout << "Child1\n"; }
    virtual void vf() { cout << "virtual: Child1\n"; }
    virtual void pvf() { cout << "pure virtual: Child1\n"; }
};

/* Child2 is-a Base */
class Child2 : public Base {
  public:
    void f() { cout << "Child2\n"; }
    virtual void pvf() { cout << "pure virtual: Child2\n"; }
};

/*
      Base
   /       \
Child1   Child2
   \       /
    GrandChild
*/
// virtual inheritance
// https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem
class GrandChild : virtual public Child1, virtual public Child2 {
};

/* Square is-a Rectangle */

int main() {
  Child1 c1{};
  Base& b1 = c1;

  Child2 c2{};
  Base& b2 = c2;

  c1.f(); // Child1::f(c1);
  b1.f(); // Base::f(b1)
  c2.f(); // Child2::f(c2);
  b2.f(); // Base::f(b2)

  c1.vf();
  b1.vf();
  c2.vf();
  b2.vf();

  c1.pvf();
  b1.pvf();
  c2.pvf();
  b2.pvf();
  
  /*
  vector<Base*> trucs_a_afficher;
  trucs_a_afficher.push_back(new Child1);
  trucs_a_afficher.push_back(new Child2);
  
  for (TrucAAfficher* truc_a_afficher : trucs_a_afficher) {
    truc_a_afficher->afficher();
    TrucAAfficher::afficher(truc_a_afficher);
  }
  */

	return 0;
}

/*
class Base:
  def f(self): print('Base')
class Child1(Base):
  def f(self): print('Child1')
  
c1 = Child1()
c1.f()
# b = Base(c1)
b.f()
*/