#include <unicode/unistr.h>

#include <cassert>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <vector>

struct TrieNode {
    std::map<UChar32, TrieNode> children{};
    bool is_word = false;

    void insert(const icu_67::UnicodeString& word, int32_t offset = 0) {
        if (offset == word.length()) {
            is_word = true;
            return;
        }
        auto [it, exists] = children.try_emplace(word.char32At(offset));
        it->second.insert(word, offset + 1);
    }

    bool contains(const icu_67::UnicodeString& word, int32_t offset = 0) const {
        if (offset == word.length()) {
            return is_word;
        }
        auto it = children.find(word.char32At(offset));
        return it != children.end() && it->second.contains(word, offset + 1);
    }

    struct CloseWords {
        const TrieNode& trie;
        const icu_67::UnicodeString word;
        size_t distance;

        struct iterator {
            CloseWords* parent;
            std::vector<std::tuple<
                const TrieNode*,
                int32_t,
                size_t,
                bool,
                std::map<UChar32, TrieNode>::const_iterator>
            > call_stack;
            std::vector<UChar32> prefix;

            iterator& operator++() {
                while (!call_stack.empty()) {
                    auto& [node, offset, distance, has_done_current_node_word, it] = call_stack.back();

                    if (offset == parent->word.length()) {
                        if (!has_done_current_node_word && node->is_word) {
                            has_done_current_node_word = true;
                            return *this;
                        } else {
                            call_stack.pop_back();
                            if (!call_stack.empty()) {
                                assert(!prefix.empty());
                                prefix.pop_back();
                            }
                        }
                    }

                    if (it == node->children.end()) {
                        call_stack.pop_back();
                        if (!call_stack.empty()) {
                            assert(!prefix.empty());
                            prefix.pop_back();
                        }
                    } else {
                        UChar32 letter = parent->word.char32At(offset);
                        const auto& [other_letter, child] = *it;
                        ++it;
                        if (other_letter == letter) {
                            call_stack.emplace_back(&child, offset + 1, distance, false, child.children.begin());
                            prefix.push_back(other_letter);
                        } else if (distance > 0) {
                            call_stack.emplace_back(&child, offset + 1, distance - 1, false, child.children.begin());
                            prefix.push_back(other_letter);
                        }
                    }
                }
                assert(prefix.size() == 0);
                return *this;
            }

            std::string operator*() const {
                std::string result;
                return icu_67::UnicodeString::fromUTF32(prefix.data(), (int32_t) prefix.size()).toUTF8String(result);
            }

            bool operator!=(const iterator& other) const {
                return parent != other.parent || call_stack != other.call_stack || prefix != other.prefix;;
            }
        };

        iterator begin() {
            iterator ret{this, {{&trie, 0, distance, false, trie.children.begin()}}, {}};
            ++ret;
            return ret;
        }

        iterator end() {
            return {this, {}, {}};
        }
    };

    CloseWords iter_close(const icu_67::UnicodeString& word, size_t distance) const {
        return {*this, word, distance};
    }
};

int main(void) {
    std::string filename{"/usr/share/dict/french"};
    std::ifstream f{filename};
    if (!f.is_open()) {
        std::cerr << "Failed to open '" << filename << "'\n";
        return 1;
    }

    TrieNode trie{};

    std::string line;
    while (std::getline(f, line)) {
        auto word = icu_67::UnicodeString::fromUTF8(line);
        trie.insert(word);
    }

    std::cout <<  "éléphant is " << (trie.contains("éléphant" ) ? "" : "not ") << "in the trie\n";
    std::cout << "cromulent is " << (trie.contains("cromulent") ? "" : "not ") << "in the trie\n";
    std::cout << "\n";

    for (const auto& word : trie.iter_close("science", 2)) {
        std::cout << word << "\n";
    }
}
