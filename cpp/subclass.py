class Base:
  def f(self): print('Base')


class Child1(Base):
  def f(self): print('Child1')


c1 = Child1()
c1.f()

# This does not make sens: _no casting_ in Python (?)
# b = Base(c1)
# b.f()
