#!/usr/bin/env python3
from time import time
from typing import Dict, Iterable, Iterator, List, Optional


class TrieNode:
    is_word = False
    # NOTE: cannot initialize in class body because of self-reference
    children: Dict[str, 'TrieNode']

    def __init__(self) -> None:
        # NOTE: need to initialize this member in constructor (see above comment)
        self.children = {}

    def insert(self, letters: Iterator[str]) -> 'TrieNode':
        try:
            letter = next(letters)
        except StopIteration:
            return self
        else:
            child = self.children.get(letter)
            if child is None:
                child = TrieNode()
                self.children[letter] = child
            return child.insert(letters)

    def find(self, letters: Iterator[str]) -> Optional['TrieNode']:
        try:
            letter = next(letters)
        except StopIteration:
            return self
        else:
            child = self.children.get(letter)
            if child is None:
                return None
            return child.find(letters)

    def iter_close(self, word: str, idx: int, prefix: List[str], distance: int) -> Iterator[str]:
        try:
            letter = word[idx]
        except IndexError:
            if self.is_word:
                yield ''.join(prefix)
        else:
            for other_letter, child in self.children.items():
                prefix.append(other_letter)
                if other_letter == letter:
                    yield from child.iter_close(word, idx + 1, prefix, distance)
                elif distance > 0:
                    yield from child.iter_close(word, idx + 1, prefix, distance - 1)
                prefix.pop()


class Trie:
    root = TrieNode()

    def add(self, word: Iterable[str]) -> None:
        node = self.root.insert(iter(word))
        node.is_word = True

    def __contains__(self, word: Iterable[str]) -> bool:
        return self.root.find(iter(word)) is not None

    def iter_prefix(self, prefix: Iterable[str]) -> Iterator[str]:
        letters = list(prefix)
        node = self.root.find(iter(letters))
        if node is None:
            return

        def aux(node: TrieNode) -> Iterator[str]:
            if node.is_word:
                yield ''.join(letters)
            for letter, child in node.children.items():
                letters.append(letter)
                yield from aux(child)
                letters.pop()
        yield from aux(node)

    def iter_close(self, word: str, distance: int) -> Iterator[str]:
        prefix: List[str] = []
        yield from self.root.iter_close(word, 0, prefix, distance)
        assert not prefix


def main() -> None:
    trie = Trie()
    with open('/usr/share/dict/french') as f:
        start = time()
        for line in f:
            word = line.strip()
            trie.add(word)
        elapsed = time() - start
    print(f'Built trie in {elapsed:.2f} s')
    print('')

    print(f'{"éléphant" in trie=}')
    print(f'{"cromulent" in trie=}')
    print('')

    for word in trie.iter_prefix('program'):
        print(word)
    print('')

    for word in trie.iter_close('science', 2):
        print(word)
    print('')


if __name__ == '__main__':
    main()
