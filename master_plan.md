# Potential points to see

- Computer Science
    - Algorithmic Complexity
        - dynamic programming
        - graph traversal & shortest path (Dijkstra, A*, Bellman-Ford, Floyd-Wharshall)
            - with abstract graphs
        - cycle detection (Floyd, Brent)
        - substring search (Knuth-Morris-Pratt, Boyer-Moore, Rabin-Karp)
        - sequence alignment (Needleman-Wunsch, Smith-Waterman, Hirschberg)
        - matching
        - stable marriages (Gale-Shapley)
        - shuffle (Fisher-Yates)
        - big-O analysis
        - complexity classes
        - Turing Machine
    - Floating Point Arithmetic, Numerical Approximations
        - Kahan summation
    - Formal Proofs
    - Compilation
    - Distributed system
    - Parallel algorithms
    - Linear Optimization
    - Cryptography
    - Machine Learning/Datascience
        - Statistic Methods
        - Genetic Algorithms
        - Neural Networks
    - Image Processing

# To See:

- Python
    - packaging namespace packages: import sub-package
    - logging

- C
    - undefined behavior

- C++:
    - virtual inheritance
    - templates
    - SFINAE
    - move semantics
        - move constructor / move assignment operator
        - std::forward / std::move
    - lambda variable capture
    - `std::optional`
    - `std::variant`
    - smart pointers
    - converting constructors & explicit conversion functions 
    - mangling & `extern "C"`

# Seen:

- Python
    - Flask
        - routing
        - blueprints
        - green threading

- DevOps
    - React?
    - Docker
    - Kubernetes (see ~/text/kubernetes.md)
        - x509 certificates
        - kubeadm
        - namespace
        - pod
        - deployment
        - service
        - role
        - rolebinding
        - ingress
    - DNS zone

- C
    - `typedef`
    - `struct`
    - `#include`
    - declaration vs definition
    - array to pointer decay
    - uninitialized variable
    - for/while/do loops
    - pointers
    - stack/heap & `malloc`/`realloc`/`free`
    - NTCS
    - input/output
    - error handling
    - OOP
    - designated initializer
    - const specifier
    - GMP
    - `char`

- C++
    - standards
    - function overloading
    - constructor overloading
    - default constructors
    - default member initialization
    - using namespace std; and using std::X;
    - most vexing parse
    - member initialization list
    - references
    - `std::string`
    - `std::cout`
    - `operator<<` overloading
    - `std::vector`
    - range-based for loop
    - lambdas
    - `<algorithm>` (count, find, count_if, find_if)
    - static variable initialization during program startup
    - constructor, destructor, scope, RAII
    - namespace name shadowing
    - access specifier (public/protected/private) for members
    - copy constructor, copy assignment operator
    - static methods
    - `struct` vs. `class`
    - method declaration, inside/outside class body
    - inheritance
    - access specifier (public/protected/private) for inheritance
    - `auto`
    - `this`
