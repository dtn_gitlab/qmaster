// Templates
// https://cppinsights.io/
#include <cmath>
#include <iostream>
#include <optional>
using namespace std;

//#define MAX_SIZE 100  // to avoid if possible
static const size_t MAX_SIZE = 100; // typing, compilation checks

template <class T>
// equivalent to
//template <typename T>
class Vector {
private:
  T data[MAX_SIZE];
  size_t current_size = 0;
public:
  void push_back(T v) {
    if (current_size < MAX_SIZE) {
      data[current_size] = v;
      current_size++;
    }
  }
  // __getitem__
  optional<T> operator[] (size_t i) const {
    if (i >= current_size) {
      return nullopt;
    }
    return data[i];
  }
  size_t size() const { return current_size; }
};

template <class T, class U>
T add(T a, U b) {
  return a + b;
}

template <>
unsigned int add(unsigned int a, double b) {
  return a + static_cast<unsigned int>(max(0., floor(b)));
}

template <>
class Vector<bool> {
private:
  uint64_t data;  // MAX_SIZE = 64
  size_t current_size = 0;
public:
  void push_back(bool v) {
    if (current_size < MAX_SIZE) {
      data |= (v << current_size);
      current_size++;
    }
  }
  // __getitem__
  optional<bool> operator[] (size_t i) const {
    if (i >= current_size) {
      return nullopt;
    }
    return data & (1 << i);
  }
  size_t size() const { return current_size; }
};

template <class T>
class MyClass {
  Vector<T>
};

/*
// Optional[Something]
std::optional<Something> try_to_find_something(void) {
  
}
*/

// instantiate a class to get an object
// instantiate a class tempalte to get a class

int main() {
  Vector<int> v;
  v.push_back(3);
  v.push_back(4);
  v.push_back(0);
  optional<int> x = v[5];
  if (x.has_value()) {
    cout << x.value() << "\n";
  } else {
    cout << "There is not spoon\n";
  }
  
  Vector<string> w;
  w.push_back("Hello");
  w.push_back("World");
  optional<string> s = w[1];
  if (s.has_value()) {
    cout << s.value() << "\n";
  } else {
    cout << "There is not spoon\n";
  }
  
  Vector<bool> booleans;
  booleans.push_back(true);
  booleans.push_back(false);
  booleans.push_back(false);
  booleans.push_back(true);
  optional<bool> b = booleans[1];
  if (b.has_value()) {
    cout << b.value() << "\n";
  } else {
    cout << "There is not spoon\n";
  }

  add(3, 4);
	return 0;
}

/* semi-equivalent in C
#define UNDERLYING_TYPE int
#include <vector.h>
#undef UNDERLYING_TYPE

#define UNDERLYING_TYPE float
#include <vector.h>
#undef UNDERLYING_TYPE
*/

/*
vector<int> v;
v[4] = 3;  // __setitem__
int& operator[](size_t i);
*/

// Zero-cost abstraction


/*
while True:
  x = y * z + w;  # __mul__

C: “no” abstraction
C++: abstraction resolved at compilation ← Rust
Python: abstraction resolved at execution ← Java, Ruby, C#
*/