#include <cstdio>

/*
// Integral types
// generally 2-complement arithmetic (from -2^(b-1) to +2^(b-1)-1)

// technically all three could be different
// generally signed
// generally 8 bits (generally signed is from -128 to 127, unsigned is from 0 to 255)
char  // sizeof(char) == 1
signed char
unsigned char

// signed by default
// can be unsigned
short  // generally 2 bytes
int  // generally 4 bytes
long  // generally 4 bytes on 32 bits and 8 bytes on 64 bits
long long  // generally 8 bytes

// Real types (in practice, floating-point IEEE 754)
float  // generally single-precision (32 bits)
double  // generally double-precision (64 bits)  // equivalent to float in Python
long double  // generally quadruple-precision (128 bits)
*/

/*
#include <stdint.h>

// work as if 2-complement X bits

// signed
int8_t
int16_t
int32_t
int64_t

// unsigned
uint8_t
uint16_t
uint32_t
uint64_t

// etc
fast_int8_t
least_int8_t
*/

/*
#include <stddef.h>

// represent a size of memory or an offset or an index
size_t  // unsigned

// non-standard
ssize_t  // signed
// annoying
*/

#include <stdio.h>
int main(void) {
  {
    unsigned int x = 1000000;
    unsigned int y = x * x;  // in practise: (x * x) % 2³²
    printf("%u\n", y);  // overflow but valid
  }

  {
    int x = 1000000;
    int y = x * x;
    printf("%d\n", y);  // UB!
  }
}
