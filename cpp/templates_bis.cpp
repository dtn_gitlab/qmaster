// CI stateless/stateful? Docker pull/push

#include <iostream>
#include <string>

template <class T>
struct Point {
  T x;
  T y;
  T z;
};

// Specialization of template
template <>
struct Point<int> {
  int x;
  int y;
  int z;
};

// passed by copy
int f_copy(int x) {  // x refers to a local copy of the original variable
  x = 3;
  return x + 2;
}
void g_copy(void) {
  int x = 0;
  f_copy(x);
  // x is unchanged
}

// passed by "copy" _of pointer_
// x is of a different type!
// int* != int
int f_pointer(int* x) {
  *x = 3;  // need to dereference pointer to modify or read value
  return *x + 2;
}
void g(void) {
  int x = 0;
  f_pointer(&x);
  // x is changed to 3
}

// passed by reference
// int& ~= int
int f_reference(int& x) {  // x refers to the original variable
  x = 3;
  return x + 2;
}
void g_reference(void) {
  int x = 0;
  f_reference(x);
  // x is changed to 3
}

// this is promise that we never modify x
// int f_reference(const int& x)

// to avoid copy, we use "const T&" instead of "T"

template <class T>
void cmp(const T& x, const T& y) {
  bool lower = x < y; // operator<()
  if (lower) {
    std::cout << "lower\n";
  } else {
    std::cout << "greater or equal\n";
  }
}

int main(void) {
  Point<float> pf = {1.f, 2.f, 3.f};
  //Point<std::string> ps = {"1", "2", "3"};

  // instantiate the cmp<int> template
  int x = 3;
  int y = 5;
  cmp(x, y);
  std::cout << "x = " << x << "\n";
  std::cout << "y = " << y << "\n";

  // instantiate the cmp<const char*> template
  //std::cout << "const char[]\n";
  //cmp("1", "2");    // outputs "greater or equal"
  // "1" = &{31 00}    (char*)
  // "2" = &{32 00}    (char*)
  // char* x;
  // char* y;
  // x < y;

  // instantiate the cmp<std::string> template
  std::string s1 = std::string("1"); // = "1", not char[]
  std::string s2 = std::string("2"); // = "2"
  std::cout << "std::string\n";
  cmp(s1, s2);  // outputs "lower"
  
  // cmp<std::string>
  // calls the cmp<const char*> template
  std::cout << "const char[]\n";
  cmp("1", "2");  // ?
}