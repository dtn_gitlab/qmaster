struct MyClass {
    int x;
};

void f1(MyClass var) {  // pass by copy
    var.x = 4;  // changes local variable
    // do something
}

void f2(MyClass& var) {  // pass by reference (like in Python)
    var.x = 4;  // changes caller's variable
    // do something
}

void f3(const MyClass& var) {  // pass by reference (like in Python)
    (void) var;
    // var.x = 4;  // will not compile
    // do something
}

void f1(MyClass var);  // copying might be expensive
void f2(MyClass& var);  // might modify object
void f3(const MyClass& var);  // perfect

void caller() {
    MyClass var{};
    f1(var);  // will not modify var
    f2(var);  // will it modify var?
    f3(var);  // will not modify var
}
