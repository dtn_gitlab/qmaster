#include <iostream>

// preprocessing, before any C or C++ syntax is parsed
#define x 2 + 3
// #define x (2 + 3)  need parentheses to avoid wrong usages (see below)

// evaluates twice the expressions x or y
#define max(x, y) ((x) > (y) ? (x) : (y))

double complex_function1() {
    std::cout << "complex_function1\n";
    return 7;
}
double complex_function2() {
    std::cout << "complex_function2\n";
    return 42;
}

int main() {
    int y = 2 * x;  // 2 * 2 + 3
    std::cout << y << "\n";  // 7

    // calls complex_function1 either complex_function2 twice
    double z = max(complex_function1(), complex_function2());
    (void) z;
}
