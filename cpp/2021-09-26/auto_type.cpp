#include <iostream>
#include <vector>

int main() {
    std::vector<int> v{1, 2, 4, 5};
    for (int x : v) {
        std::cout << x << "\n";
    }

    // same as

    for (auto i = v.begin(); i != v.end(); ++i) {
        std::cout << *i << "\n";
    }

    // same as

    for (std::vector<int>::iterator i = v.begin(); i != v.end(); ++i) {
        std::cout << *i << "\n";
    }

    // Herb Sutter → “Almost Always Auto”
    auto x{3};  // int
    (void) x;
}

// mostly useful for generic programming
// makes it easier to write very flexible code for generic programming
template<typename T>
size_t count_elements(T v) {
    size_t n = 0;
    auto i = v.begin();
    while (i != v.end()) {
        ++n;
        ++i;
    }
    return n;
}
