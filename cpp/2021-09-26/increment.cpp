#include <iostream>

int main() {
    // pre-increment
    {
        int a = 2;
        int x = ++a;
        std::cout << "pre-increment: " << x << "\n";  // 3
        // a = 3
    }

    // post-increment
    {
        int a = 2;
        int x = a++;
        std::cout << "post-increment: " << x << "\n"; // 2
        // a = 3
    }

    // prefer ++a instead of a++ to avoid unnecessary copies of complex objects:

    /*
    // post-increment
    V& operator++(int unused) {
        V ret = *this;  // this is a V*
        this->increment();
        return ret;
    }

    int x = a++;
    // 1. expression "a++"" returns a copy of a
    // 2. increment a
    // 3. put the returned value into x

    // pre-increment
    V& operator++() {
        this->increment;
        return *this;
    }
    int x = ++a;
    // 1. increment a
    // 2. expression "++a" returns a
    // 3. put the returned value into x
    */
}
