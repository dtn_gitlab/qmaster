#include <iostream>

using namespace std;

// this is a virtual class because of vritual_method
// this is an abstract class because of pure_vritual_method
class MyParentClass {
public:
  int y;
  
  void non_virtual_method(void) {
    cout << "MyParentClass::non_virtual_method()\n";
  }
  
  // virtual: can be implemented in sub classes
  virtual void virtual_method(void) {
    cout << "MyParentClass::virtual_method()\n"; // fallback if not implemented in subclasses
  }

  virtual void pure_virtual_method(void) = 0; // pure virtual: must be implemented in subclasses
  
  /*
  struct {
    void (*MyParentClass::virtual_method_ptr)(void); // pointer to method of MyParentClass
  } vtable;
  
  MyParentClass(void) {
    vtable.virtual_method_ptr = MyParentClass::virtual_method;
    vtable.pure_virtual_method = ?;
  }
  */
};

/*
Abstract class inheritance tree example
MyParentClass
→ MyClass1  // implements pure_virtual_method → can be instanced (MyClass1 thing1)
→ MyClass2
→ MyClass3  // does not implement pure_virtual_method → cannot be instanced yet
  → MySubClass31  // implements pure_virtual_method → can be instanced (MySubClass31 subthing31)
*/

class MyClass1 : public MyParentClass { // heritage
public:
  int x;
  
  void something(void) {
    cout << "Hello 1\n";
  }

  void non_virtual_method(void) {
    cout << "MyClass1::non_virtual_method()\n";
  }
  
  void virtual_method(void) {
    cout << "MyClass1::virtual_method()\n";
  }
    
  void pure_virtual_method(void) {
    cout << "MyClass1::pure_virtual_method\n";
  }
  
  /*
  MyClass1(void) {
    vtable.virtual_method_ptr = MyClass1::virtual_method;
  }
  */
};

class MyClass2 : public MyParentClass {
public:
  int x;

  void something(void) {
    cout << "Hello 2\n";
  }

  void non_virtual_method(void) {
    cout << "MyClass2::non_virtual_method()\n";
  }
  
  /*
  void virtual_method(void) {
    cout << "MyClass2::virtual_method()\n";
  }
  */
    
  void pure_virtual_method(void) {
    cout << "MyClass2::pure_virtual_method\n";
  }
};

void f(MyParentClass* bla) {
  bla->virtual_method();
  //bla->virtual_method();
  /*
  MyClass1* thing1 = dynamic_cast<MyClass1*>(bla); // check at compile time
  if (thing1 == nullptr) {  // equivalent to NULL in C++
    return;
  }
  cout << "thing1->x = " << thing1->x << "\n";
  */
}

int main(void) {
  MyClass1 thing1;
  thing1.virtual_method();
  f(&thing1);
  MyClass2 thing2;
  thing2.virtual_method();
  f(&thing2);
}
