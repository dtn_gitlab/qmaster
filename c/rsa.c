#include <stdio.h>
#include <gmp.h>
// https://gmplib.org/manual/
// typedef int[45] mpz_t;

void genprime(mpz_t p, gmp_randstate_t state) {
    // Prime proba algos:
    // https://en.wikipedia.org/wiki/Primality_test#Probabilistic_tests
    // https://en.wikipedia.org/wiki/AKS_primality_test

    do {
        mpz_urandomb(p, state, 1024);
    } while (!mpz_probab_prime_p(p, 40));
    // 4^(-reps) → 1 / 4096 → 1 / 2¹² → 1 / 4⁶
}

int main(void) {
    mpz_t p, q, n, lam_n, p_minus_1, q_minus_1, d, e;
    mpz_inits(p, q, n, lam_n, p_minus_1, q_minus_1, d, e, NULL);

    // seed the state with entropy (e.g. /dev/urandom)
    // Python → os.urandom(42)

    gmp_randstate_t state;
    gmp_randinit_default(state);

    genprime(p, state);
    genprime(q, state);


    // RSA: https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Operation
    // Quantum Comp: Shor's algorithm, reducing comp time for the attacker

    // n = p × q
    mpz_mul(n, p, q);

    // lam_n = lcm(p - 1, q -1)
    mpz_sub_ui(p_minus_1, p, 1);
    mpz_sub_ui(q_minus_1, q, 1);
    mpz_lcm(lam_n, p_minus_1, q_minus_1);

    // e = 65,537
    mpz_set_ui(e, 65537);

    // d = e⁻¹ (mod lam_n)
    mpz_invert(d, e, lam_n);


    printf("p = ");
    mpz_out_str(stdout, 10, p);
    puts("");

    printf("q = ");
    mpz_out_str(stdout, 10, q);
    puts("");

    printf("n = ");
    mpz_out_str(stdout, 10, n);
    puts("");

    printf("e = ");
    mpz_out_str(stdout, 10, e);
    puts("");

    printf("d = ");
    mpz_out_str(stdout, 10, d);
    puts("");


    mpz_clears(p, q, n, lam_n, p_minus_1, q_minus_1, d, e, NULL);
}
