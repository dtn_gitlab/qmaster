#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Point3D = struct Point3D
// automatic in C++
// typedef struct Point3D Point3D;

// Point3D = struct { ... }
/*
typedef struct {
    // ...
} Point3D;
*/

// Point3D = struct Point3D { ... }
/*
typedef struct Point3D {
    // ...
} Point3D;
*/

struct Point3D {
    int x;
    int y;
    int z;

/*
    char* buffer;

    struct Blabla {
        int depth;
        float aze;
    } blabla;
*/
};


// Méthode en C
struct Point3D point3d_sum(const struct Point3D* a, const struct Point3D* b) {
    return (struct Point3D) {
        .x = a->x + b->x,
        .y = a->y + b->y,
        .z = a->z + b->z
    };
}

/*
const int buffer_size = 4;

struct Point3D point3d_copy(struct Point3D* a) {
    char* new_buffer = malloc(buffer_size);
    memcpy(new_buffer, a->buffer, buffer_size);
    return (struct Point3D) {
        .x = a->x,
        .y = a->y,
        .z = a->z,
        .buffer = new_buffer,
    }
}
*/

void test_point(void) {
    struct Point3D point1;
    point1.x = 3;
    point1.y = 4;
    point1.z = 5;

    struct Point3D point2 = {
        1, 2, 3,
    };

    struct Point3D point3 = {
        .x = 42,
        .y = 24,
        .z = 46,
    };

    point3 = point3d_sum(&point1, &point2);
    printf("x=%d y=%d z=%d\n", point3.x, point3.y, point3.z);
    printf("x=%d y=%d z=%d\n", point1.x, point1.y, point1.z);
}

void g(int* x) {
    printf("%zu\n", sizeof(x));
    printf("%d\n", x[4]);
}

// int, float, T*, struct {}, T[]
void f(void) {
    int myarray[42];  // 42 * sizeof(int) = 168
    printf("%zu\n", sizeof(myarray));
    g(myarray);
}

struct DynArray {
    int* data;
    int size;
};


struct ChainedList {
    float v;
    struct ChainedList* next;
};

struct DynArray* dyn_new(void) {
    // https://man7.org/linux/man-pages/man3/malloc.3.html
    // void *malloc(size_t size);
    struct DynArray* dyn = malloc(sizeof(struct DynArray));
    if (dyn == NULL) {
        printf("Not enough memory!\n");
        exit(1);
    }
    dyn->size = 42;
    dyn->data = malloc(dyn->size * sizeof(int));
    if (dyn->data == NULL) {
        printf("Not enough memory!!!\n");
        exit(1);
    }
    return dyn;
}

void dyn_del(struct DynArray* dyn) {
    free(dyn->data);
    free(dyn);
}

int dyn_getitem(struct DynArray* dyn, int i) {
    if (i >= dyn->size) {
        printf("Index too big! (%d >= %d)\n", i, dyn->size);
        exit(1);
    }
    return dyn->data[i];
}

void dyn_setitem(struct DynArray* dyn, int i, int v) {
    if (i >= dyn->size) {
        dyn->size = i * 2;
        dyn->data = realloc(dyn->data, dyn->size * sizeof(int));
        if (dyn->data == NULL) {
            printf("Not enough memory!!!!!!!!!\n");
            exit(1);
        }
    }
    dyn->data[i] = v;
}

void dyn_delitem(struct DynArray* dyn, int i);  // __setitem__

int main(void) {
    struct DynArray* dyn = dyn_new();

    for (int i = 0; i < 8000000; i++) {
        dyn_setitem(dyn, i, 2 * i);
    }
    printf("%d\n", dyn_getitem(dyn, 500));

    dyn_del(dyn);
}



/*
vector<int> v;

v.push_back(1);
v.push_back(2);
v.push_back(3);
v.push_back(4);
v.push_back(5);

v.size() == 5;

== 10

struct DynArray {
    int* data;
    int available;
    int size;
};


*/